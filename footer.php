
		</div><!-- #content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="block_row" style="height: auto; padding-top: 40px;">
				<div class="glblock_third" style="align-content: start">
					<p>Pyesmead Self Catering,<br>
					Pyesmead Farm,<br>
					Plaitford,<br>
					Hampshire,<br>
					SO51 6EE</p>
				</div>
				<div class="glblock_third" style="align-content: start">
					<ul>
						<li><a href="#">The Farm</a></li>
						<li><a href="#">Shepherd's Pye</a></li>
						<li><a href="#">The Cottage</a></li>
						<li><a href="#">Gallery</a></li>
						<li><a href="#">Contact</a></li>
					<ul>
				</div>
				<div class="glblock_third" style="align-content: start">
					<ul>
						<li><a href="#">Email</a></li>
						<li><a href="#">Phone</a></li>
						<li><a href="#">Facebook</a></li>
						<li><a href="#">Instagram</a></li>
					<ul>
				</div>
			</div>
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
