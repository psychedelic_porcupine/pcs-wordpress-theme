<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="grid-container" id="home-grid-1">
  <div class="grid-item" id="grid-item-1"></div>
  <div class="grid-item" id="grid-item-2">
    <p>Come &amp; stay on a beautiful working farm situated on the edge of the New forest</p>
  </div>
  <div class="grid-item" id="grid-item-3"></div>
  <div class="grid-item" id="grid-item-4"></div>
  <div class="grid-item" id="grid-item-5"></div>
  <div class="grid-item" id="grid-item-6"></div>
  <div class="grid-item" id="grid-item-7"></div>
  <div class="grid-item" id="grid-item-8">
  <p>Shepherd's Pye is a stunning shepherd hut converted from a 1920's railway carriage</p>
  </div>
  <div class="grid-item" id="grid-item-9"></div>
  <div class="grid-item" id="grid-item-10"></div>
  <div class="grid-item" id="grid-item-11"></div>
  <div class="grid-item" id="grid-item-12"></div>
</div>

<?php
get_footer();
